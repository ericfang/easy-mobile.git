const state = {
    navigateDirect: 0,
};

// getters
const getters = {
    transitionName: (state) => {
        switch (state.navigateDirect) {
            case -1:
                return 'slide-right';
            case 1 :
                return 'slide-left';
            default:
                return '';
        }
    }
};

// actions
const actions = {};

// mutations
const mutations = {
    setNavigateDirect(state, value) {
        state.navigateDirect = value;
    }
};


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}