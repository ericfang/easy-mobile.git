import {_import} from "@/utils/common.util";

export default [
    {
        path: '/',
        component: _import('index/Index'),
        meta: {
            title: "首页",
        },
        children: [
            {
                path: '',
                component: _import('route-demo/RouteDemoIndex'),
            },
            {
                path: '/RequestDemoIndex',
                component: _import('request-demo/RequestDemoIndex'),
            },
            {
                path: '/BetterScrollDemoIndex',
                component: _import('better-scroll-demo/BetterScrollDemoIndex'),
            }
        ]
    },
    {
        path: '/RouteDemoNormalBack',
        component: _import('route-demo/RouteDemoNormalBack'),
    },
    {
        path: '/RouteDemoHookBack',
        component: _import('route-demo/RouteDemoHookBack'),
    },
    {
        path: '/RouteDemoBackSkip',
        component: _import('route-demo/RouteDemoBackSkip'),
    },
    {
        path: '/RouteDemoBackSkip2',
        component: _import('route-demo/RouteDemoBackSkip2'),
    },
    {
        path: '/RouteDemoInherit',
        component: _import('route-demo/RouteDemoInherit'),
    },
    {
        path: '/RouteDemoInherit2',
        component: _import('route-demo/RouteDemoInherit2'),
    },
    {
        path: '/RouteDemoInherit3',
        component: _import('route-demo/RouteDemoInherit3'),
    },
    {
        path: '/RouteDemoHookSkip',
        component: _import('route-demo/RouteDemoHookSkip'),
    },
    {
        path: '/RouteDemoHookSkip2',
        component: _import('route-demo/RouteDemoHookSkip2'),
    },
    {
        path: '/RouteDemoWaitTask',
        component: _import('route-demo/RouteDemoWaitTask'),
    },
    {
        path: '/BetterScrollNormal',
        component: _import('better-scroll-demo/BetterScrollNormal'),
    },
    {
        path: '/BetterScrollPullDown',
        component: _import('better-scroll-demo/BetterScrollPullDown'),
    },
    {
        path: '/BetterScrollAll',
        component: _import('better-scroll-demo/BetterScrollAll'),
    },
    {
        path: '/BetterScrollPos',
        component: _import('better-scroll-demo/BetterScrollPos'),
    },
    {
        path: '/BetterScrollPos2',
        component: _import('better-scroll-demo/BetterScrollPos2'),
    },
    {
        path: '/RequestDemoNormal',
        component: _import('request-demo/RequestDemoNormal'),
    },
]