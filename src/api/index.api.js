import {request} from "@/utils/request.util";

export const getIndexList = (params) => {
    return request({
        url: '/index/list',
        methods: 'get',
        params,
    })
};