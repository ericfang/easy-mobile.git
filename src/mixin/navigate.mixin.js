import {mapState} from "vuex";
import {Events} from "@/utils/navigate.util";
import {debThr} from "@/utils/common.util";

export const NavigateMixin = {
    data() {
        return {
            // 强制刷新，无论是返回还是打开
            NAV_FORCE_RELOAD: false,
            // 是否已经 MOUNTED 处理；
            NAV_MOUNTED: false,
            // 初始化执行延迟时间（尽量和切换动画时间一致，防止动画在执行js时卡顿）
            NAV_INIT_DELAY: 400,
            // 返回钩子函数执行延时
            NAV_BACK_HOOK_DELAY: 1,
            // 缓存钩子函数
            NAV_BACK_INVOKE: null,

        }
    },
    computed: {
        ...mapState('layout', ['navigateDirect'])
    },
    created() {
    },
    mounted() {
        this.NAV_MOUNTED = true;
        this.INIT_NAVIGATE_PAGE();
    },
    activated() {
        if (this.NAV_MOUNTED) {
            return this.NAV_MOUNTED = false;
        }
        this.INIT_NAVIGATE_PAGE();
    },
    deactivated() {

    },
    destroy() {
    },
    methods: {
        INIT_NAVIGATE_PAGE() {
            if (this.navigateDirect >= 0 || this.NAV_FORCE_RELOAD) {
                setTimeout(this.init, this.NAV_INIT_DELAY);
                return;
            }
            this.backInit();
            if (this.PAGE_AUTO_BACK) {
                return this.BACK_SKIP();
            }
            this.NAV_BACK_INVOKE = Events.backInvoke;
            const {funcName, params} = this.NAV_BACK_INVOKE || {};
            setTimeout(() => {
                this[funcName] && this[funcName](...params);
                Events.clearBackInvoke();
                this.NAV_BACK_INVOKE = null;
            }, this.NAV_BACK_HOOK_DELAY);
            // debThr.debounce('NavigateMixinClearBackInvoke', Events.clearBackInvoke.bind(Events), 100);
        },

        init() {
        },
        backInit(){
        }
    }
};

export const PageMixin = {
    mixins: [NavigateMixin],
    data() {
        return {
            // 自动返回，当页面为返回情况下，自动再次返回到上一页
            PAGE_AUTO_BACK: false,
            // 返回跳过继承，上一页如果是返回跳过的话是否本页也继承跳过
            PAGE_BACK_SKIP_INHERIT: false,

        }
    },

    methods: {
        BACK_SKIP(...params) {
            if (this.PAGE_BACK_SKIP_INHERIT || this.PAGE_AUTO_BACK) {
                this.$navigate.backInvoke("BACK_SKIP", ...params);
            }
        },
    }

};