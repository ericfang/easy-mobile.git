const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    outputDir: './dist',
    publicPath: './',
    lintOnSave: false,
    productionSourceMap: false,
    chainWebpack: (config) => {
        config.resolve.alias
            .set('@', resolve('src'));
        // config.entry.app = ["babel-polyfill", "./src/main.js"];
    },
    devServer: {
        // 端口配置
        port: 2007,
        // 反向代理配置
        proxy: {
            '/api': {
                target: 'http://localhost:8086',
                ws: true,
                pathRewrite: {
                    '^/api/ezm-server': '',
                }
            }
        }
    }
};
